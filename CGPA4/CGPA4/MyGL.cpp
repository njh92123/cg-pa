#include "MyGL.h"
#include <glm/gtc/matrix_access.hpp>
#include <algorithm>
//------------------------------------------------------------------------------
MyGL::MyGL()
    :
    _doTriangulate( false ),
    _doRasterize( false ) {
}

//------------------------------------------------------------------------------
MyGL::~MyGL()
{}

//------------------------------------------------------------------------------
bool MyGL::TriangulatePolygon( const vector<GLVertex> &polygonVerts,
                               vector<GLVertex> &triangleVerts ) {
    if ( !_doTriangulate )
        return false;

    //
    // YOUR CODE HERE
    //

    // Implement triangulation here.
    // Keep in mind that the color of the first scene (F1) will depend on your implementation.
    // You must set it right so that it should not feel so "glitchy".

    if ( polygonVerts.size() >= 3 ) {
		for (int i = 0; i < (int)polygonVerts.size() - 2; i++) {
			triangleVerts.push_back(polygonVerts[0]);
			triangleVerts.push_back(polygonVerts[i + 1]);
			triangleVerts.push_back(polygonVerts[i + 2]);
		}
        return true;
    } else {
        return true;
    }
}

//------------------------------------------------------------------------------
bool MyGL::RasterizeTriangle(GLVertex verts[3]) {
	if (!_doRasterize)
		return false;

	//
	// YOUR CODE HERE
	//

	// Implement rasterization here. You are NOT required to implement wireframe mode (i.e. 'w' key mode).
	GLVertex v0 = verts[0];
	GLVertex v1 = verts[1];
	GLVertex v2 = verts[2];

	float xCoefficients[3] = {
		v0.position[1] - v1.position[1], // y0 - y1
		v1.position[1] - v2.position[1],
		v2.position[1] - v0.position[1]
	};

	float yCoefficients[3] = {
		v1.position[0] - v0.position[0], // x1 - x0
		v2.position[0] - v1.position[0],
		v0.position[0] - v2.position[0]
	};

	float constantTerms[3] = {
		v0.position[0] * v1.position[1] - v1.position[0] * v0.position[1], // x0y1 - x1y0
		v1.position[0] * v2.position[1] - v2.position[0] * v1.position[1],
		v2.position[0] * v0.position[1] - v0.position[0] * v2.position[1]
	};

	int width, height;
	frameBuffer.GetSize(width, height);

	std::pair<int, int> xRange = std::minmax({ (int)v0.position[0], (int)v1.position[0], (int)v2.position[0] });
	std::pair<int, int> yRange = std::minmax({ (int)v0.position[1], (int)v1.position[1], (int)v2.position[1] });

	xRange.first = std::max(xRange.first, 0);
	xRange.second = std::min(xRange.second, width);
	yRange.first = std::max(yRange.first, 0);
	yRange.second = std::min(yRange.second, height);

	for (int x = xRange.first; x < xRange.second; x++) {
		for (int y = yRange.first; y < yRange.second; y++) {
			bool isInEdge = true;
			for (int i = 0; i < 3; i++) {
				if (xCoefficients[i] * x + yCoefficients[i] * y + constantTerms[i] < 0) {
					isInEdge = false;
					break;
				}
			}

			if (!isInEdge) {
				// (x, y)가 삼각형 안에 들어있지 않으면 패스
				continue;
			}

			// 알고 있는 정보는 v0, v1, v2의 color이고 알고 싶은 건 (x, y)의 color
			// v0, v1을 지나는 직선 L1과 v2, (x, y)를 지나는 직선 L2의 교점 (p, q)를 구함
			// v0, v1 사이의 interpolation으로 (p, q)의 color를 구함
			// v2, (p, q) 사이의 interpolation으로 (x, y)의 color를 구함
			glm::vec4 color;
			float p, q;

			// v0, v1을 지나는 직선은 xCoefficients[0], yCoefficients[0], constantTerms[0]에 이미 있음
			// ax+by+c = 0 L2를 구함
			float a = v2.position[1] - y; // L2의 xCoefficient y2 - y
			float b = x - v2.position[0]; // L2의 yCoefficient x - x2
			float c = v2.position[0] * y - x * v2.position[1]; // L2의 constant term x2 * y - x * y2

			// p, q를 구함
			assert(xCoefficients[0] * b != a * yCoefficients[0]); // L1과 L2의 기울기는 달라야함
			p = (yCoefficients[0] * c - b * constantTerms[0]) / (xCoefficients[0] * b - a * yCoefficients[0]);

			if (yCoefficients[0] == 0) {
				// L2에 p 대입
				q = (-a * p - c) / b;
			} else {
				// L1에 p 대입
				q = -xCoefficients[0] / yCoefficients[0] * a - constantTerms[0] / yCoefficients[0];
			}

			// v0, v1 사이의 interpolation으로 (p, q)의 Z-Buffer를 구함
			float z = Interpolate(p, q, v0.position.x, v0.position.y, v1.position.x, v1.position.y, v0.position.z, v1.position.z);

			// v2, (p, q) 사이의 interpolation으로 (x, y)의 Z-Buffer를 구함
			z = Interpolate(x, y, v2.position.x, v2.position.y, p, q, v2.position.z, z);

			float oldZ = frameBuffer.GetDepth(x, y);
			if (z > oldZ) {
				// (x, y)의 Z-Buffer가 기존 값보다 크면 패스
				continue;
			}
			if (z > 1 || z < 0) {
				// (x, y)의 Z-Buffer가 정상 범위를 벗어나면 패스
				continue;
			}

			// v0, v1 사이의 interpolation으로 (p, q)의 color를 구함
			color.x = Interpolate(p, q, v0.position.x, v0.position.y, v1.position.x, v1.position.y, v0.color.x, v1.color.x);
			color.y = Interpolate(p, q, v0.position.x, v0.position.y, v1.position.x, v1.position.y, v0.color.y, v1.color.y);
			color.z = Interpolate(p, q, v0.position.x, v0.position.y, v1.position.x, v1.position.y, v0.color.z, v1.color.z);
			color.w = Interpolate(p, q, v0.position.x, v0.position.y, v1.position.x, v1.position.y, v0.color.w, v1.color.w);

			// v2, (p, q) 사이의 interpolation으로 (x, y)의 color를 구함
			color.x = Interpolate(x, y, v2.position.x, v2.position.y, p, q, v2.color.x, color.x);
			color.y = Interpolate(x, y, v2.position.x, v2.position.y, p, q, v2.color.y, color.y);
			color.z = Interpolate(x, y, v2.position.x, v2.position.y, p, q, v2.color.z, color.z);
			color.w = Interpolate(x, y, v2.position.x, v2.position.y, p, q, v2.color.w, color.w);

			if (color.x == 0.2f && color.y == 0.2f && color.z == 0.4f) {
				// 배경
			} else {
				frameBuffer.SetDepth(x, y, z);
			}

			frameBuffer.SetPixel(x, y, color);
		}
	}

	return true;
}

float MyGL::Interpolate(float x, float y, float fromX, float fromY, float toX, float toY, float fromValue, float toValue)
{
	if (fromX == toX) {
		assert(fromY != toY);
		return fromValue + (toValue - fromValue) * std::abs((y - fromY) / (fromY - toY));
	} else {
		assert(fromX != toX);
		return fromValue + (toValue - fromValue) * std::abs((x - fromX) / (fromX - toX));
	}
}

